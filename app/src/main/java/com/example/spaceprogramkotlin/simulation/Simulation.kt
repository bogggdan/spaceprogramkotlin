package com.example.spaceprogramkotlin.simulation

import android.os.Build
import androidx.annotation.RequiresApi
import com.example.spaceprogramkotlin.model.Item
import com.example.spaceprogramkotlin.model.Rocket
import com.example.spaceprogramkotlin.spaceship.U1
import com.example.spaceprogramkotlin.spaceship.U2
import java.util.*
import java.util.stream.Collectors

class Simulation {
    @RequiresApi(Build.VERSION_CODES.N)
    fun loadU1(items: List<Item>) : ArrayList<U1> {
        val cargo = items.stream().sorted { t, t2 -> t.weight.compareTo(t2.weight) * -1 }.collect(
            Collectors.toList())
        val spaceships = ArrayList<U1>()

        while (cargo.isNotEmpty()) {
            val rocket = U1()

            if (!rocket.canCarry(cargo.last())) {
                break
            }

            for (item in cargo) {
                if (rocket.canCarry(item)) {
                    rocket.carry(item)
                }
            }

            cargo.removeAll(rocket.cargo)
            spaceships.add(rocket)
        }

        return spaceships
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun loadU2(items: List<Item>) : ArrayList<U2> {
        val cargo = items.stream().sorted { t, t2 -> t.weight.compareTo(t2.weight) * -1 }.collect(
            Collectors.toList())
        val spaceships = ArrayList<U2>()

        while (cargo.isNotEmpty()) {
            val rocket = U2()

            if (!rocket.canCarry(cargo.last())) {
                break
            }

            for (item in cargo) {
                if (rocket.canCarry(item)) {
                    rocket.carry(item)
                }
            }

            cargo.removeAll(rocket.cargo)
            spaceships.add(rocket)
        }

        return spaceships
    }

    fun runSimulation(spaceships: List<Rocket>): Int {
        var budget = 0
        for (rocket in spaceships) {
            do {
                budget += rocket.cost
            } while (!rocket.launch() || !rocket.land())
        }
        return budget
    }
}