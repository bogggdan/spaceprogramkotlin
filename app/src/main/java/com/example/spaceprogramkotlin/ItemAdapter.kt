package com.example.spaceprogramkotlin

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import com.example.spaceprogramkotlin.model.Item

class ItemAdapter(context: Context, private val items: MutableList<Item>) : BaseAdapter() {

    private val lInflater: LayoutInflater = LayoutInflater.from(context)

        override fun getCount(): Int {
        return items.size
    }

    override fun getItem(p0: Int): Any {
        return items[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = lInflater.inflate(R.layout.item, p2, false)
        val p = getItem(p0) as Item

        view.findViewById<TextView>(R.id.name).text = p.name
        view.findViewById<TextView>(R.id.weight).text = p.weight.toString() + "kg"
        val button = view.findViewById<Button>(R.id.delete_button)
        button.tag = p0

        button.setOnClickListener {
            val index:Int = it.tag as Int
            items.removeAt(index)
            notifyDataSetChanged()
        }

        return view;
    }
}