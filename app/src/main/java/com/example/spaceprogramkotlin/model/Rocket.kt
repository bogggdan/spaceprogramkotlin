package com.example.spaceprogramkotlin.model

import android.os.Parcel
import android.os.Parcelable

open class Rocket(val cost: Int, val weight: Int, val maxWeight: Int) : SpaceShip, Parcelable {
    val cargo: ArrayList<Item> = ArrayList()

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun launch(): Boolean {
        return true
    }

    override fun land(): Boolean {
        return true
    }

    override fun canCarry(item: Item): Boolean {
        return getWeightLeft() >= item.weight
    }

    override fun carry(item: Item) {
        cargo.add(item)
    }

    fun countCargoWeight() : Int {
        var weight = 0

        for(it in cargo) {
            weight += it.weight
        }

        return weight
    }

    fun getWeightLeft() : Int {
        return maxWeight - weight - countCargoWeight()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(cost)
        parcel.writeInt(weight)
        parcel.writeInt(maxWeight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rocket> {
        override fun createFromParcel(parcel: Parcel): Rocket {
            return Rocket(parcel)
        }

        override fun newArray(size: Int): Array<Rocket?> {
            return arrayOfNulls(size)
        }
    }
}