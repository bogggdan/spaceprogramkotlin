package com.example.spaceprogramkotlin.spaceship

import com.example.spaceprogramkotlin.model.Rocket

class U1 : Rocket(100, 10000, 18000) {
    override fun launch(): Boolean {
        return Math.random() * 100 > (5 * (countCargoWeight().toDouble() / (maxWeight - weight)))
    }

    override fun land(): Boolean {
        return Math.random() * 100 > (countCargoWeight().toDouble() / (maxWeight - weight))
    }
}