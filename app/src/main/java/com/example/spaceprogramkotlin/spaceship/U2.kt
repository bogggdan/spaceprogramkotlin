package com.example.spaceprogramkotlin.spaceship

import com.example.spaceprogramkotlin.model.Rocket

class U2 : Rocket(120, 18000, 29000) {
    override fun launch(): Boolean {
        return Math.random() * 100 > (4 * (countCargoWeight().toDouble() / (maxWeight - weight)))
    }

    override fun land(): Boolean {
        return Math.random() * 100 > (8 * (countCargoWeight().toDouble() / (maxWeight - weight)))
    }
}