package com.example.spaceprogramkotlin

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.spaceprogramkotlin.model.Item
import com.example.spaceprogramkotlin.simulation.Simulation
import com.example.spaceprogramkotlin.spaceship.U1
import com.example.spaceprogramkotlin.spaceship.U2
import java.util.*

class MainActivity : AppCompatActivity() {
    private var items: ArrayList<Item> = ArrayList<Item>()
    private val simulation: Simulation = Simulation()
    private var spaceshipsU1: ArrayList<U1> = ArrayList<U1>()
    private var spaceshipsU2: ArrayList<U2> = ArrayList<U2>()
    private var adapter: ItemAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState != null) {
            items = savedInstanceState.getParcelableArrayList<Item>("items") as ArrayList<Item>
            spaceshipsU1 = savedInstanceState.getParcelableArrayList<U1>("u1") as ArrayList<U1>
            spaceshipsU2 = savedInstanceState.getParcelableArrayList<U2>("u2") as ArrayList<U2>
        }

        setContentView(R.layout.activity_main)
        val listView = findViewById<ListView>(R.id.items)
        this.adapter = ItemAdapter(this, items)
        listView.adapter = adapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList("items", items)
        outState.putParcelableArrayList("u1", spaceshipsU1)
        outState.putParcelableArrayList("u2", spaceshipsU2)
        super.onSaveInstanceState(outState)
    }

    fun addClick(view: View?) {
        val name = findViewById<TextView>(R.id.item_name)
        val weight = findViewById<TextView>(R.id.item_weight)
        if (name.text.toString().isEmpty() || weight.text.toString().isEmpty()) {
            Toast.makeText(this, getText(R.string.empty_toast), Toast.LENGTH_LONG).show()
        } else {
            items.add(Item(name.text.toString(), Integer.decode(weight.text.toString())))
            adapter!!.notifyDataSetChanged()
        }
        name.editableText.clear()
        weight.editableText.clear()
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    fun fillU1Click(view: View?) {
        spaceshipsU1 = simulation.loadU1(items)
    }

    fun clearClick(view: View?) {
        items.clear()
        spaceshipsU2.clear()
        spaceshipsU1.clear()
        adapter!!.notifyDataSetChanged()
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    fun fillU2Click(view: View?) {
        spaceshipsU2 = simulation.loadU2(items)
    }

    fun startClick(view: View?) {
        if (spaceshipsU1.isEmpty() && spaceshipsU2.isEmpty()) {
            Toast.makeText(this, getText(R.string.rockets_toast), Toast.LENGTH_LONG).show()
            return
        }
        val totalU1: Int = simulation.runSimulation(spaceshipsU1)
        val totalU2: Int = simulation.runSimulation(spaceshipsU2)
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Space program cost")
            .setMessage(
                """
            For u1 rockets: ${totalU1}million$
            For u2 rockets: ${totalU2}million$
            """
            )
            .setPositiveButton("OK") { dialog: DialogInterface, id: Int -> dialog.cancel() }
        builder.show()
    }

}